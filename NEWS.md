# brclimr 0.0.2

* Remove ggplot2 package suggestion.

# brclimr 0.0.1

* fetch_data function retrieves data from BRDWGD zonal product.
* Added a `NEWS.md` file to track changes to the package.
